datos_in = op('base_datos')
datos_fixed = op('chopto1')
posiciones = op('posiciones')
resultados = op('resultados')
reset = op('reset')[0]

def isclose(a, b, rel_tol=1e-09, abs_tol=0.002):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def depuracion(datos_in_,datos_fixed_,posiciones_,resultados_):
    resultados_.setSize(datos_in.numRows,2)
    for i in range(datos_fixed_.numRows):
        for x in range(1,datos_in_.numRows): 
            a = datos_in_[x,0]
            b = datos_fixed_[i,0]
            if isclose(a,b) == True:
                resultados_[x,0] = posiciones_[i,0]
                resultados_[x,1] = posiciones_[i,1]
                #print(posiciones_[i,0])
            elif reset == 1:
                resultados_[x,0] = 0
                resultados_[x,1] = 0      
                #print(x) 
    return

def _main_():
    depuracion(datos_in,datos_fixed,posiciones,resultados)
    return