# me - this DAT
# scriptOp - the OP which is cooking

import numpy as np
import cv2 as cv
import math
#path = me.var('TOUCH') + '/data/scripts/xml/haarcascade_eye.xml'
datos = op('huMoments')
colores_datos = op('colores_datos')
crop_poicionesX = op('crop_posicionesX')
crop_poicionesY = op('crop_posicionesY')

# press 'Setup Parameters' in the OP to call this function to re-create the parameters.
def onSetupParameters(scriptOp):
	return

# called whenever custom pulse parameter is pushed
def onPulse(par):
	return

def onCook(scriptOp):
	img = op('fichas').numpyArray(delayed=True)
	gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
	gray = (gray * 255).astype(np.uint8)
	'''
	car_cascade = cv.CascadeClassifier(path)
	cars = car_cascade.detectMultiScale(gray,1.5,1)
	cars = np.int0(cars)
	'''
	ret,thresh = cv.threshold(gray,127,255,int(op('thres')[0]))
	contours,h = cv.findContours(thresh,1,2)
	count_ = 0
	datos.clear()
	colores_datos.clear()	
	crop_poicionesX.clear()
	crop_poicionesY.clear()
	for cnt in contours:
		approx = cv.approxPolyDP(cnt,0.01*cv.arcLength(cnt,True),True)
		#Aqui sacamos el centro de las figuras para colocar texto
		M = cv.moments(approx)
		#Con esto sacamos los HuMoments leer teoria
		huMoments = cv.HuMoments(M)
		#Log scale hu moments
		cX = int((M["m10"] / M["m00"]))
		cY = int((M["m01"] / M["m00"]))
		
		colores_ = [255,0]

		theta = 0
		th = theta * np.pi / 180
		e = np.array([[np.cos(th), np.sin(th)]]).T
		es = np.array([
		[np.cos(th), np.sin(th)],
		[np.sin(th), np.cos(th)],
		]).T
		dists = np.dot(cnt,es)
		wh = dists.max(axis=0) - dists.min(axis=0)
		#print("==> theta: {}\n{}".format(theta, wh))
		#MUCHO OJO con esta madre ya que le estas diciendo que puede
		#detectar patrones que van mas aya de 3 puntos
		#tienes que hacer pruebas con esto
		if len(approx) >= 3:
			for i in range(0,7):
				#Aqui se manifiestas los datos del HuMoments
				try:
					huMoments[i] = -1* math.copysign(1.0,huMoments[i])*math.log10(abs(huMoments[i]))
					#print(huMoments[0])
				except:
					o = 0	
			id_ = round(math.acos(M['nu02']),3)
			cv.drawContours(img,[cnt],0,colores_[0],-1)
			count_ += 1 
			cv.putText(img, str(count_), (cX+50, cY), cv.FONT_HERSHEY_DUPLEX, 0.7, (255, 255, 255), 1)
			#/////////////////////////////////////////////////
			#Recuerda que aqui estas multiplicando x2 ya que 
			#la resolucion la estas partiendo a la mitad
			#para no tener problemas de performance
			crop_poicionesX.appendRows([cX*2])
			crop_poicionesY.appendRows([cY*2])
			datos.appendRows([huMoments[0]])	
		'''
		elif len(approx) == 4:
			cv.drawContours(img,[cnt],0,(255,100,0),-1)
		elif len(approx) == 5:
			cv.drawContours(img,[cnt],0,(0,0,255),-1)					
		'''			
	#OJO con esto aqui agrego de forma manual los puntos basicamente agarro el rgb
	#de cada crop, esto lo hago manual ya que el settr no me funiona
	#entonces recuerda que aqui va la cantidad maxima de crop
	#El maximo de fichas pueden ser 30 si quieres mas modifica los replicatos de crop_colores
	try:
		crp1 = [round(op('crop_colores/crop1').sample(x=25,y=100)[0],3),round(op('crop_colores/crop1').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop1').sample(x=25,v=0.5)[2],3)]
		crp2 = [round(op('crop_colores/crop2').sample(x=25,y=100)[0],3),round(op('crop_colores/crop2').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop2').sample(x=25,v=0.5)[2],3)]
		crp3 = [round(op('crop_colores/crop3').sample(x=25,y=100)[0],3),round(op('crop_colores/crop3').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop3').sample(x=25,v=0.5)[2],3)]		
		crp4 = [round(op('crop_colores/crop4').sample(x=25,y=100)[0],3),round(op('crop_colores/crop4').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop4').sample(x=25,v=0.5)[2],3)]		
		crp5 = [round(op('crop_colores/crop5').sample(x=25,y=100)[0],3),round(op('crop_colores/crop5').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop5').sample(x=25,v=0.5)[2],3)]		
		crp6 = [round(op('crop_colores/crop6').sample(x=25,y=100)[0],3),round(op('crop_colores/crop6').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop6').sample(x=25,v=0.5)[2],3)]		
		crp7 = [round(op('crop_colores/crop7').sample(x=25,y=100)[0],3),round(op('crop_colores/crop7').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop7').sample(x=25,v=0.5)[2],3)]		
		crp8 = [round(op('crop_colores/crop8').sample(x=25,y=100)[0],3),round(op('crop_colores/crop8').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop8').sample(x=25,v=0.5)[2],3)]		
		crp9 = [round(op('crop_colores/crop9').sample(x=25,y=100)[0],3),round(op('crop_colores/crop9').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop9').sample(x=25,v=0.5)[2],3)]		
		crp10 = [round(op('crop_colores/crop10').sample(x=25,y=100)[0],3),round(op('crop_colores/crop10').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop10').sample(x=25,v=0.5)[2],3)]		
		crp11 = [round(op('crop_colores/crop11').sample(x=25,y=100)[0],3),round(op('crop_colores/crop11').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop11').sample(x=25,v=0.5)[2],3)]		
		crp12 = [round(op('crop_colores/crop12').sample(x=25,y=100)[0],3),round(op('crop_colores/crop12').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop12').sample(x=25,v=0.5)[2],3)]		
		crp13 = [round(op('crop_colores/crop13').sample(x=25,y=100)[0],3),round(op('crop_colores/crop13').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop13').sample(x=25,v=0.5)[2],3)]		
		crp14 = [round(op('crop_colores/crop14').sample(x=25,y=100)[0],3),round(op('crop_colores/crop14').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop14').sample(x=25,v=0.5)[2],3)]		
		crp15 = [round(op('crop_colores/crop15').sample(x=25,y=100)[0],3),round(op('crop_colores/crop15').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop15').sample(x=25,v=0.5)[2],3)]		
		crp16 = [round(op('crop_colores/crop16').sample(x=25,y=100)[0],3),round(op('crop_colores/crop16').sample(u=0.5,v=0.5)[1],3),round(op('crop_colores/crop16').sample(x=25,v=0.5)[2],3)]		
		
		#aqui tan solo agregue las 12 fichas que ocuparemos en pabellon
		colores_datos.appendRows([crp1,crp2,crp3,crp4,crp5,crp6,crp7,crp8,crp9,crp10,crp11,crp12])
	except:
		l = 0
	
	#print(round(op('crop_colores/crop1').sample(x=25,y=100)[0],3))

	
	scriptOp.copyNumpyArray(img)	
	return
