puntos = op('blobtrack1_blobs')
distancia_resultados = op('distancia_resultados')

def distancias(puntos_, tolerancia_X,tolerancia_Y, resultados_):
    valoresX = []
    valoresY = []
    resultados_.setSize(0,0)

    for i in range(puntos_.numRows-1):
        i += 1
        conversionX_ = round(float(puntos_[i,1]),3)
        conversionY_ = round(float(puntos_[i,2]),3)
        valoresX.append(conversionX_)
        valoresY.append(conversionY_)
    
    count = 0
    for i in range(len(valoresX)):
        count += 1
        depuracion_ = []
        try:
            if i == count-1:
                for x in range(len(valoresX)):
                    comparacion_X = abs(round((valoresX[i] - valoresX[i+x]),3))
                    comparacion_Y = abs(round((valoresY[i] - valoresY[i+x]),3))
                    #print(str(comparacion_X) + ' //// ' + str(comparacion_Y))
                    if comparacion_X < tolerancia_X and comparacion_Y < tolerancia_Y:
                        depuracion_.append(str(i+1) + ',' + str((i+x)+1))
                        #print(str(valoresX[i]) + ' - ' + str(valoresX[i+x]) + ' = ' + str(comparacion_X) + ' /// ' + str((i+x)+1))
                        #print(str(valoresY[i]) + ' - ' + str(valoresY[i+x]) + ' = ' + str(comparacion_Y) + ' /// ' + str((i+x)+1))
                    #print(str(depuracion_) + '  len: ' + str(len(depuracion_)))    
                    #/////////////////////////////////////////////OJO CON ESTO YA QUE SOLO FUNCIONA PARA FIGURAS CON 3 PUNTOS //////////////////////////
                    if len(depuracion_) == 3:
                        for z in range(len(depuracion_)):
                            if depuracion_[z] != depuracion_[0]:
                                p = 0
                                #print(z -1)
                        resultados_.insertRow(str(depuracion_[0]) + '/' + str(depuracion_[1]) + '/' + str(depuracion_[2]))  
                    #print(valores[i+x])
        except:
            l = 0
            #print('>>>>>>> fuera de rango <<<<<<<<<')
    return

def _main_():
    toleranciaX = 0.060
    toleranciaY = 0.111
    distancias(puntos,toleranciaX,toleranciaY,distancia_resultados)
    return