import numpy as np
from scipy.signal import savgol_filter

datos = op('datos_')
resultados = op('res')

def onFrameStart(frame):
    size_datos = datos.numRows
    array_ = np.random.rand(size_datos,size_datos)
    for i in range(size_datos):
        array_[0:] = datos[i,0]
        array_[1:] = datos[i,1]
        array_[2:] = datos[i,2]
        array_[3:] = datos[i,3]
        array_[4:] = datos[i,4]
        array_[5:] = datos[i,5]
        array_[6:] = datos[i,6]
        array_[7:] = datos[i,7]
        array_[8:] = datos[i,8]
        array_[9:] = datos[i,9]
        array_[10:] = datos[i,10]
        array_[11:] = datos[i,11]


    resultados.setSize(0,0)    

    data = array_
    z = savgol_filter(data,size_datos+1,6,mode='nearest')

    resultados.appendRows([z[0],z[1],z[2],z[3],z[4],z[5],z[6],z[7],z[8],z[9],z[10],z[11]])


    return