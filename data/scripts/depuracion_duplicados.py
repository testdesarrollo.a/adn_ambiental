depuracion = op('depuracion')
resultados_depuracion = op('resultados_depuracion')

def depuracion_duplicados(depuracion_, resultados_):
    size_tabla = []
    for i in range(depuracion_.numRows):
        if depuracion_[i,0] != depuracion_[i-1,0]:
            size_tabla.append(i)
    resultados_.setSize(len(size_tabla),3)
    for i in range(len(size_tabla)):
        resultados_[i,0] = depuracion_[size_tabla[i],0]
        resultados_[i,1] = depuracion_[size_tabla[i],1]
        resultados_[i,2] = depuracion_[size_tabla[i],2]

    return

def _main_():
    depuracion_duplicados(depuracion,resultados_depuracion)    
    return