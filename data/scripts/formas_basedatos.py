import math

base_datos = op('base_datos')
datos_tiempo_real = op('datos_tiempo_real')
id_color = op('identificador_color')

def comparacion(datos_base_,real_,id_color_):
    count = 0
    id_color_.setSize(1,1)
    for i in range(datos_base_.numRows):
        count += 1
        try:
            for x in range(datos_base_.numRows):
                redondeando_base = round(float(datos_base_[x,0]),3)
                redondeando_real = round(float(real_[count-1,0]),3)
                #print(str(redondeando_base) + ' //// ' + str(redondeando_real))
                if math.isclose(redondeando_base,redondeando_real,abs_tol=0.2) == True:
                    #print('econtre el similar: ' + str(x))
                    #print(datos_base_[x,1],datos_base_[x,2],datos_base_[x,3])

                    id_color_.appendRows([datos_base_[x,1],datos_base_[x,2],datos_base_[x,3]])
        except:
            a = 0
            #print('')
    return

def _main_():
    comparacion(base_datos,datos_tiempo_real,id_color)
    return