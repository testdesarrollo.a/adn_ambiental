# me - this DAT
# scriptOp - the OP which is cooking

import numpy as np
import cv2 as cv
import math
#path = me.var('TOUCH') + '/data/scripts/xml/haarcascade_eye.xml'
datos = op('table3')
posicionesX = op('posiciones_timepo_realX')
posicionesY = op('posiciones_timepo_realY')
# press 'Setup Parameters' in the OP to call this function to re-create the parameters.
def onSetupParameters(scriptOp):
	return

# called whenever custom pulse parameter is pushed
def onPulse(par):
	return

def onCook(scriptOp):
	img = op('tiempo_real').numpyArray(delayed=True)
	gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
	gray = (gray * 255).astype(np.uint8)
	'''
	car_cascade = cv.CascadeClassifier(path)
	cars = car_cascade.detectMultiScale(gray,1.5,1)
	cars = np.int0(cars)
	'''
	ret,thresh = cv.threshold(gray,127,255,int(op('thres')[0]))
	contours,h = cv.findContours(thresh,cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
	
	datos.clear()	
	posicionesX.clear()
	posicionesY.clear()
	for cnt in contours:
		approx = cv.approxPolyDP(cnt,0.01*cv.arcLength(cnt,True),True)
		#Aqui sacamos el centro de las figuras para colocar texto
		M = cv.moments(approx)
		#Con esto sacamos los HuMoments leer teoria
		huMoments = cv.HuMoments(M)
		# Log scale hu moments
		cX = int((M["m10"] / M["m00"]))
		cY = int((M["m01"] / M["m00"]))
			
		theta = 0	
		th = theta * np.pi / 180
		e = np.array([[np.cos(th), np.sin(th)]]).T
		es = np.array([
		[np.cos(th), np.sin(th)],
		[np.sin(th), np.cos(th)],
		]).T
		dists = np.dot(cnt,es)
		wh = dists.max(axis=0) - dists.min(axis=0)
		#print("==> theta: {}\n{}".format(theta, wh))
		#print(cnt,len(approx))
		#MUCHO OJO con esta madre ya que le estas diciendo que puede
		#detectar patrones que van mas aya de 3 puntos
		#tienes que hacer pruebas con esto
		if len(approx) >= 3:
			for i in range(0,7):
				#Aqui se manifiestas los datos del HuMoments
				try:
					huMoments[i] = -1* math.copysign(1.0,huMoments[i])*math.log10(abs(huMoments[i]))
				except:
					print('error en opencv_tiempo_real.py')
				#print(huMoments[0])
			id_ = round(math.acos(M['nu02']),3)
			cv.drawContours(img,[cnt],0,255,-1)
			#print(huMoments[0])
			#cv.putText(img, str(huMoments[1]), (cX, cY), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)
			
			_, triangle = cv.minEnclosingTriangle(cnt)
			pts = np.int32(np.squeeze(np.round(triangle)))
			#print('\nminEnclosingTriangle: \n', pts)
			# Refine/improve triangle vertices (if wanted)
			idx = [np.argmin(np.linalg.norm(cnt - pt, axis=2)) for pt in pts]
			pts = np.int32(np.squeeze(cnt[idx]))
			#print('\nRefined: \n', pts)
			# Find index of "top" vertex by finding unique edge lengths; "top" has less than the two base vertices
			idx = np.argmin([np.size(np.unique(np.linalg.norm(pts - pt, axis=1))) for pt in pts])
			top = pts[idx]
			#print(top)
			# Find mid point of the base vertices
			base = np.array([pts[i] for i in np.arange(3) if i != idx])
			base_mid = np.int32(np.round(np.mean(base, axis=0)))
			# Draw angle bisector line
			#img = cv.line(img, tuple(top), tuple(base_mid) , (0, 255, 0), 2)

			rect = cv.minAreaRect(cnt)
			(x, y), (width, height), angle = rect
			aspect_ratio = min(width, height) / max(width, height)
			#print(aspect_ratio)

			#Recuerda que aqui estas multiplicando x2 ya que 
			#la resolucion la estas partiendo a la mitad
			#para no tener problemas de performance
			posicionesX.appendRows([cX*2])
			posicionesY.appendRows([cY*2])						
			#datos[0,0] = float(huMoments[1])
			datos.appendRows([huMoments[0]])	
		else:
			a = 0
			#print('no se detecto algo')
		'''
		elif len(approx) == 4:
			cv.drawContours(img,[cnt],0,(255,100,0),-1)
		elif len(approx) == 5:
			cv.drawContours(img,[cnt],0,(0,0,255),-1)					
		'''			
	scriptOp.copyNumpyArray(img)	
	return
